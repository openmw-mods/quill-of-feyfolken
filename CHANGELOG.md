# CHANGELOG

## [2.0.5]
Updated to use delta_plugin 0.15 (required).

## [2.0.1]
Fixed bugs in scripts to make compatible with OpenMW.

## [2.0]
Fixed bug where player's magicka would be modified permanently.
Changed the way the quill is acquired to minimize conflicts.
Added new mesh and icon for the quill.

## [1.1]
The scrolls for sale were put into containers rather than on the person.

## [1.0]
Released.
