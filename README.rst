==============
Quill of Feyfolken
==============
Version 2.0.5

Originally By Dormouse.
See http://mw.modhistory.com/download--13440

Development Status: Maintenance. Patches and improvements will be accepted, but this is not actively developed.

Requires the use of `DeltaPlugin <https://gitlab.com/bmwinger/delta-plugin>`_ 0.15 (or greater) to create the esp. See the `Releases page <https://gitlab.com/openmw-mods/quill-of-feyfolken/-/releases>`_ for archives containing the pre-compiled esp.

Introduction
****************
Every enchanter knows the tale of the Quill of Feyfolken. Everything written with the quill posesses magical qualities. If one is granted its power, it is rumored they would be able to use the soul of Feyfolken to inscribe powerful scrolls effortlessly. Before you set out to find the Quill of Feyfolken, perhaps you should first read of his tale.


Features
***********
This is a scroll making mod that will make enchanting scrolls a viable option in Morrowind. Without this mod, enchanting scrolls is just as difficult as enchanting jewelry, but they can only be cast once. This makes enchanting scrolls pointless. The purpose of this mod is to counter these limitations.

There is a small quest involved to receive the Quill of Feyfolken. Once it is in your possession, it can be equipped to enchant scrolls. It has two benefits. It has the soul of Feyfolken, which is replenished by absorbing magicka. While equipped, it also mods the user's enchant level to ten times the original.

To ensure the mod is balanced, I included numerous lines in script to ensure that these benefits can only be used when enchanting scrolls. If you try to enchant something other than scrolls or if you equip the quill to gain the level bonus, but use a different soul, you will lose the quill.

The scrolls that can be enchanted were made and distributed to four enchanters across Morrowind. There are different levels of blank scrolls with enchantment points from relatively low to very high. The power of the scroll you can produce is therefore limited only by your enchantment skill, which must be well past 100 to use the most powerful scrolls.

The mesh for the quill is from Qarl's Misc Item Replacer. I have modified the texture to give it a unique look from other quill pens.

Please read the Issues section below.

Gameplay Details
***********************
The Quill of Feyfolken can be obtained by collecting all three volumes of the story "Feyfolken". Once you have all three in your possession, you can find the quill in the shack Saryoni lived in while writing his sermons (Llarala's Shack).

There are six levels of blank scrolls. They are sold depending on your level, except if you become Telvanni Archmagister. The four enchanters that sell them are Hlendrisa Seleth (Tel Uvirith), Miun-Gei (Vivec, FQ), Llether Vari (Ald'Ruhn), and Galar Rothan (Sadrith Mora).

If you want to just test the mod without collecting the books and finding the scrolls, the console IDs for the items are as follows:

* misc_quill_feyfolken
* bk_FeyfolkenMessage
* sc_blank1 ... sc_blank6


Compatibility
*****************
As of version 2.0, there should not be any compatibility issues. If anyone has trouble with the mod, please let me know.


Credit
********
All credit for the quill pen mesh goes to Qarl. It is from Qarl's Misc Item Replacer. Thanks!


Installation
**************
Requires:
Morrowind

Move the esp file and folders to the Data Files folder.

If you are upgrading from an older version, run Morrowind without the old version, save your game, and reload Morrowind with the updated version. You will need to get the quill again.


Issues
********
The mod has been tested repeatedly, and there should be little to no bugs. However, these issues still remain:
* If you have more than one filled Grand Soul Gem in your inventory, the enchant screen will open with one of your original soul gems in the gem slot. Simply add the Grand Soul Gem (Feyfolken) to the gem slot instead. If the quill is used without the proper soul gem, you will lose the quill.
* Due to the script that adds an empty Azura's Star to the player's inventory when used, I was required to add the condition that the quill could not be used if the player has Azura's Star in their inventory. You just need to place Azura's Star on the floor or elsewhere while making scrolls.
* If anyone has any issues, whether from serious bugs (shouldn't be any) to grammar mistakes, please let me know so I can fix them.

Contact
**********
The original Author's email address is dormouse_mods at yahoo.com

Copyright
**********
The original mod was licensed using the following statement:

    If you want to use this mod or parts of it for your own, please do, but give credit where it is due.

This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
